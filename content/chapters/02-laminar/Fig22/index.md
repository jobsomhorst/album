---
title: "Fig 21. Axisymmetric flow past a Rankine ogive"
date: 2020-08-12
draft: true
tags: ["Finite volume", "OpenFoam"]
authors:
  - "steinstoter"
---

This is the body of revolution that would be produced by a point potential source in a uniform stream - the axisymmetric counterpart of the plane half-body of figure 2. Its shape is so gentle that at zero incidence and a Reynolds number of 6000 based on its diameter the flow remains attached and laminar. Streamlines are made visible by tiny air bubbles in water illuminated by a sheet of light in the mid-plane. *ONERA photograph, Werlé 1962*