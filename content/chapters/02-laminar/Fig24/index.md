---
title: "Fig 24. Circular cylinder at R=1.54"
date: 2020-10-08
weight: 24
tags: ["StarCCM", "Laminar", "FlowPastCylinder", "Separation"]
authors:
  - "jobsomhorst"
---

{{< katex >}}

{{< slider "Featured.png" "new24.png" "Experiment" "Simulation">}}
*At this Reynolds number the streamline pattern has clearly lost the fore-and-aft symmetry of figure 6. However, the flow has not yet separated at the rear. That begins at about R=5, though the value is not known accurately. Streamlines are made visible by aluminum powder in water.* Photograph by Sadathoshi Taneda

## General Info
This post is part of a series of posts about flow past a circular cylinder at different Reynolds numbers. In this post, it is clearly visible that the flow is still attached to the cylinder and therefore separation did not occur yet. Separation only starts occurring from a Reynolds number of approximately 5. More information about separation can also be found in the [web post from Figure 40]({{< ref "/chapters/03-Separation/Fig40" >}}).

The CFD simulation program used to perform the simulation of this figure is Simcenter STARCCM+. The visualization of the figure is done with Paraview, mainly using the Streamtracer filter. 720 particles are inserted at the circumference of the circle to colour it white. For this, a Matlab script is used in combination with a TableToPoints filter. 

For this visualization, no Particletracer filter is used because the flow is still attached to the cylinder and therefore there are no vortices to visualize. The complete Paraview pipeline in which all used filters are shown can be found below. 

More information about the simulation and visualization can also be found in the [web post from Figure 40]({{< ref "/chapters/03-Separation/Fig40" >}}).

{{< figure     
    src="pipeline24.png"
    caption="Paraview pipeline"
    alt="fig24 pipeline"
    >}}
