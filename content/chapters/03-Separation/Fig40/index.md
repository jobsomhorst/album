---
title: "Fig 40. Circular cylinder at R=9.6"
date: 2020-10-08
weight: 40
tags: ["StarCCM", "Laminar", "FlowPastCylinder", "Separation"]
authors:
  - "jobsomhorst"
---

{{< katex >}}

{{< slider "Featured.jpg" "new40.1.png" "Experiment" "Simulation">}}
*Here, in contrast to figure 24, the flow has clearly separated to form a pair of recirculating eddies. The cylinder is moving through a tank of water containing aluminum powder, and is illuminated by a sheet of light below the tree surface. Extrapolation of such experiments to unbounded flow suggests separation at R=4 or 5, whereas most numerical computations give R=5 to 7.* Photograph by Sadathoshi Taneda

## Theory

#### Laminar, steady-state flow

The flow past a circular cylinder is a well-studied case in the fluid dynamics department. The flow pattern behind the cylinder changes when the Reynolds number of the flow changes. In the case of Figure 40, the Reynolds number is still relatively low, which represents a laminar, steady-state flow. The flow behind a circular cylinder stays steady-state until a Reynolds number of approximately 40 is reached. [Figure 46]({{< ref "/chapters/03-Separation/Fig46" >}}) represents the upper limit of steady-state flow behind a circular cylinder.

Laminar flow is characterized by a smooth flow which consists of infinitesimal parallel layers with no disruption between them. The flow layers move without swirls or eddies in parallel to each other. The flow layers will never cross and flow particles will follow the same trajectory. This means that laminar flow, in comparison to the complex and irregular turbulent flow, is easy to model. The flow can be described by the simplified Navier-Stokes equation.

#### Navier-Stokes equation
The Navier-Stokes equation is a set of coupled differential equations that describe the relationship between the velocity, pressure, density and temperature of a moving viscous fluid. The compressible Navier-Stokes equation is derived from the Cauchy stress tensor and the linear stress constitutive equation shown below.

$$
\begin{cases} 
\rho \frac{Du}{Dt} = -\nabla p +\nabla \cdot \tau +\rho g \\\\
\sigma = \zeta(\nabla \cdot u)I + \mu[(\nabla u + (\nabla u)^T - \frac{2}{3}(\nabla \cdot u))I] 
\end{cases}
$$

Combining these two equations results in the most general form of the Navier-Stokes equation:
$$
\rho \frac{Du}{Dt} = \rho (\frac{\partial u}{\partial t} +(u \cdot \nabla)u) = -\nabla p + \nabla \cdot \{ \mu [\nabla u +(\nabla u)^T - \frac{2}{3}(\nabla \cdot u)I + \zeta (\nabla \cdot u)I] \} + \rho g
$$

To simplify the equation, one can use the Stokes hypothesis. The Stokes hypothesis is the assumption that the volume viscosity is equal to zero \\(\zeta = 0\\), causing the \\(+\zeta (\nabla \cdot u)I\\) term at the right-hand side to disappear. Another assumption that can be made is that the dynamic viscosity \\(\mu\\) can be assumed to be constant. After taking the divergence of the tensors \\(\nabla u\\) and \\((\nabla u)^T\\) as being \\(\nabla^2 u\\) and \\(\nabla (\nabla u)\\) respectively, one gets the following equation:

$$
\rho \frac{\partial v}{\partial t} +\rho (v \cdot \nabla)v = \nabla p + \mu \nabla^2 v + \frac{1}{3}\mu \nabla (\nabla \cdot u) + \rho g
$$

As for the figures in this series one considers incompressible flow (constant density) and it holds that for incompressible flow \\(\nabla \cdot u = 0\\), the Navier-Stokes equation above changes into the simplified Navier-Stokes equation for incompressible flow, which can be seen below. 

$$
rho \frac{\partial v}{\partial t} +\rho (v \cdot \nabla)v + -\nabla p = \mu \nabla^2 v + \rho g
$$

Note that for a steady-state system, the time-derivative also becomes \\(0\\). 

The equations below show the Navier-Stokes equation written out in a Cartesian coordinate system. This shows that the Navier-Stokes equation works in all three velocity directions.

$$
\begin{cases}
x: \rho (\frac{\partial u_x}{\partial t} + u_x \frac{\partial u_x}{\partial x} + u_y \frac{\partial u_x}{\partial y} + u_z \frac{\partial u_x}{\partial z}) = -\frac{\partial p}{\partial x} + \mu (\frac{\partial^2 u_x}{\partial x^2} +\frac{\partial^2 u_x}{\partial y^2} +\frac{\partial^2 u_x}{\partial z^2}) + \rho g_x \\\\
y: \rho (\frac{\partial u_y}{\partial t} + u_x \frac{\partial u_y}{\partial x} + u_y \frac{\partial u_y}{\partial y} + u_z \frac{\partial u_y}{\partial z}) = -\frac{\partial p}{\partial y} + \mu (\frac{\partial^2 u_y}{\partial x^2} +\frac{\partial^2 u_y}{\partial y^2} +\frac{\partial^2 u_y}{\partial z^2}) + \rho g_y \\\\
z: \rho (\frac{\partial u_z}{\partial t} + u_x \frac{\partial u_z}{\partial x} + u_y \frac{\partial u_z}{\partial y} + u_z \frac{\partial u_z}{\partial z}) = -\frac{\partial p}{\partial z} + \mu (\frac{\partial^2 u_z}{\partial x^2} +\frac{\partial^2 u_z}{\partial y^2} +\frac{\partial^2 u_z}{\partial z^2}) + \rho g_z
\end{cases}
$$

If we again assume the density to remain constant in the system (incompressible flow) and the flow to be steady-state (dropping the time derivative), the Navier-Stokes equation simply change into the set of coupled Equations below.

$$
\begin{cases}
x:  \mu (\frac{\partial^2 u_x}{\partial x^2} +\frac{\partial^2 u_x}{\partial y^2} +\frac{\partial^2 u_x}{\partial z^2}) -\frac{\partial p}{\partial x} = 0 \\\\
y:  \mu (\frac{\partial^2 u_y}{\partial x^2} +\frac{\partial^2 u_y}{\partial y^2} +\frac{\partial^2 u_y}{\partial z^2}) -\frac{\partial p}{\partial y} = 0 \\\\
z:  \mu (\frac{\partial^2 u_z}{\partial x^2} +\frac{\partial^2 u_z}{\partial y^2} +\frac{\partial^2 u_z}{\partial z^2}) -\frac{\partial p}{\partial z} = 0
\end{cases}
$$

In this case, because of incompressibility, it also holds that:

$$
\frac{\partial u_x}{\partial x} + \frac{\partial u_y}{\partial y} + \frac{\partial u_z}{\partial z} = 0
$$

From the Navier-Stokes equation, one can directly derive the Reynolds number, which is a non-dimensional number that gives you the ratio between inertial and viscous forces. To derive the Reynolds number, one should first have a look at the final representation of the Navier-Stokes equations again:

$$
\rho \frac{\partial v}{\partial t} +\rho (v \cdot \nabla)v + -\nabla p = \mu \nabla^2 v + \rho g
$$

The Navier-Stokes equation can be non-dimensionalized by using the dimensionless variables shown below, together with the Reynolds number (\\(Re = UL/v\\)) and the Froude number (\\(Fr = \frac{u}{\sqrt{gL}}\\)).

$$
\begin{cases} 
\text{dimensionless variable for the spatial coordinate:} r^\star = \frac{r}{L} \\\\
\text{dimensionless variable for the gradient operator:} \nabla^\star = L\nabla\\\\
\text{dimensionless variable for velocity:} u^\star = \frac{u}{U}\\\\
\text{dimensionless variable for pressure at high velocity:} p^\star = \frac{pL}{\mu U}\\\\
\text{dimensionless variable for pressure at low velocity (creeping flows):} p^\star = \frac{p}{pU^2}
\end{cases}
$$

After combining the above equations, one gets the following equation as the non-dimensionalized Navier-Stokes equation for high-velocity flows:

$$
\frac{\partial u^\star}{\partial t^\star} + (u^\star \cdot \nabla^\star)u^\star = -\nabla^\star p^\star + \frac{1}{Re} \nabla^{\star 2} u^\star + \frac{1}{Fr^2} g
$$

And the following as the non-dimensionalized Navier-Stokes equation for low-velocity (or viscous) flows.

$$
Re(\frac{\partial u^\star}{\partial t^\star} + (u^\star \cdot \nabla^\star)u^\star) = -\nabla^\star p^\star +\nabla^{\star 2} u^\star
$$

For the figure treated in this section and all figures related to this section, the equation for viscous flows can be used to derive the Reynolds number because we are dealing with viscous flows.

#### Vorticity equation
The vorticity equation describes the change in the vorticity of a fluid particle as it moves with a flow. The vorticity is defined as the curl of the flow velocity vector, which is an important aspect in vortex-dominated flows. Thus, making it important to calculate when numerically describing the flow behind a circular cylinder.

The equation for the vorticity can be found by taking the curl of the Navier-Stokes equation and can be found in the equation below.
$$
\nabla \times \{ \frac{\partial u}{\partial t} + (u \cdot \nabla) u\} = \nabla \times \{ -\frac{1}{\rho} \nabla P + v\nabla^2 u \}
$$

Because the curl of a gradient is 0, one gets \\(\nabla \times \{ -\frac{\nabla P}{\rho} \} = 0\\), \\(\nabla \times \{\frac{\partial u}{\partial t} \} = \frac{\partial \omega}{\partial t} \\) and \\(\nabla \times \{v\nabla^2u \} = v\nabla^2\omega\\). The vorticity equation becomes:
$$
\frac{\partial \omega}{\partial t} + (u \cdot \nabla) \omega = (\omega \cdot \nabla)u + v \nabla^2 \omega
$$

If one compares the vorticity equation to the Navier-Stokes equation, it can be noticed that there is an extra term on the right-hand side of this equation \\((\omega \cdot \nabla)u\\). This term is called the vortex stretching term and represents the extension of a vortex in a three-dimensional fluid. In a two-dimensional case, the vortex stretching term becomes zero. This means that for this 2D case, the vortex stretching term is assumed to be irrelevant. However, when solving the problem in 3D, the vortex stretching term should be taken into account.

#### Separation
As mentioned in the caption, from a Reynolds number of approximately 4, separation at the rear of the cylinder begins to take place. Flow separation is the detachment of a boundary layer from the downstream of the cylinder surface, forming two vortices in opposite directions. Separation happens when a fluid particle reverses its direction when the velocity of the particle decreases to 0 somewhere downstream. The decrease in velocity is caused by a negative pressure gradient. The separation point and angle at which the flow separates with respect to the cylinder (separation angle) change with the Reynolds number. Later downstream, the separated flow will reattach again (reattachment point). The area between the separation point and the reattachment point is called the separation bubble. Inside this bubble, the flow will be formed into two symmetric eddies. The length of this bubble is again dependent on the Reynolds number. 

The relationship between the Reynolds number and the separation angle has been a part of numerous studies, as can be seen in the figure below.

{{< figure     
    src="angleRe.jpeg"
    caption="Relationship Re and the separation angle (Jiang 2020)"
    alt="Re vs Seperation angle"
    >}}

At Reynolds numbers smaller than 5, there is a creeping flow which does not separate from the surface of the cylinder. This can be seen in the figure below (a simulation at \\(Re=4\\)) and in the web post from Figure 1.

{{< figure     
    src="Re4.1.jpg"
    caption="Flow around a circular cylinder at \\(Re=4\\)"
    alt="Re = 4"
    >}}

After a Reynolds number of approximately \\(5\\), separation of flow starts to appear. The figure above shows that all experiments and studies considered show that when increasing the Reynolds number, the separation point moves upstream. A study by Wu et al looked into the separation angle for 2D flows to a Reynolds number of 2008 shows, through simulations and experiments, that the relationship between the separation angle and Reynolds number can be described by the following formula:
$$
\theta_S = 95.7 + 267.1Re^{-\frac{1}{2}} - 625.9Re^{-1} +1046.6Re^{\frac{-3}{2}}
$$

For a Reynolds number of \\(9.6\\), as we show in this figure, this would mean that the separation angle should be equal to \\(152\\) degrees. This corresponds with the experiment and therefore also with the simulation.

After the Reynolds number exceeds approximately \\(40\\), the steady-state flow becomes an unsteady-state flow because of the disappearing symmetry between the upstream and downstream. After this point, the separation point changes back and forth over time, due to vortex shedding. More information about this phenomenon can be found in Figure 94. 

## Simulation
The CFD simulation program used to perform the above simulations is Simcenter StarCCM+. The flow models used in the simulations are valid for 2D cases, thus the simulations are run in 2D to decrease the computational time.

#### Boundary conditions and computational domain
Only the Reynolds number (\\(Re=9.6\\)) was given in the caption of the original figure. However, it is also known that the photograph has been made by Sadathoshi Taneda, who made multiple pictures that are shown in the original book. In other pictures, Sadathoshi Taneda used a cylinder with a diameter of \\(1 cm\\), therefore it was implied that he used the same experimental setup for this photograph as well. From here, the velocity could be calculated, \\(Re = \frac{\rho u D}{\mu}, u = 0.00096 m/s\\). From here, the computational domain and boundary conditions were created. The computational domain can be seen in the figure below. The total domain has a length of \\(5 cm\\) and the distance from the centre of the cylinder to the end of the domain is \\(3 cm\\), leaving \\(2 cm\\) in front of the centre. The height of the domain is \\(3 cm\\), with the centre of the cylinder at \\(1,5 cm\\). The total domain of the Figure was not given in the caption but derived from the original figure. The solution of the simulation is not affected by the scale of the domain, therefore the top and bottom of the domain are set as symmetry planes. The cylinder is set as a wall with a no-slip boundary condition applied to it. The inflow is set as a velocity inlet with a velocity of \\(0.00096 m/s\\) and the outlet is set as a pressure outlet.

{{< figure     
    src="computational domain 40-46.png"
    caption="Computational domain"
    alt="Computational domain 40"
    >}}

#### Meshing
The mesh created for this simulation is a quadrilateral 2D mesh, which is one of many meshing options from STARCCM+. After selecting the quadrilateral 2D mesh, the polygonal mesh option is used. This mesh consists of polyhedral-shaped cells. In comparison to an equivalent tetrahedral mesh, a polygonal mesh contains five times fewer cells and is more accurate, more stable and less diffusive. The base size of the mesh is set to \\(0.02 m\\) and the growth rate to 1.004. A prism layer mesh is created next to the cylinder. Because of this, the accuracy is higher closer to the cylinder. A total amount of 34386 cells were created. Some representations of the mesh can be found below.

{{< carousel images="mesh*.jpg" interval="3000">}}

#### STARCCM+ setup
The simulation was run with a laminar, steady model with water as the flowing fluid. For the simulation, a total of 3500 iterations were simulated, of which the velocity data was exported to an Ensight Gold case file, so it could be processed in Paraview. All models used in STARCCM+ and the STARCCM+ result after 3500 iterations can be found below. 

{{< carousel images="40*.jpg" interval="3000">}}

## Visualization
Paraview has been used for the post-processing and visualization of the simulation. After importing the velocity data, a calculator filter has been used to compute the velocity vector from the x-velocity and y-velocity data that were exported. After this, the circumference of the cylinder is covered in particles with the code below. In combination with a TableToPoints filter, this is used as an input seed for the Particletracer filter. The Particletracer filter together with the Streamtracer filter is used to compute the final simulation shown at the start of this web post. The code used for creating the particles at the circumference of the cylinder, and the Paraview pipeline can be found below. 

```
n = 720;                                

angles = linspace(0, 2*pi, n);          
radius = 0.00501;                       
xCenter = 0;                            
yCenter = 0;
X = -radius * cos(angles) + xCenter;   
Y = -radius * sin(angles) + yCenter;

P = zeros(n+1,3);                      
P(1,:) = [1,2,3];                      
P(2:end,1) = X;                       
P(2:end,2) = Y;                       

writematrix(P,'circumference.csv') 
```
{{< figure     
    src="pipeline40.1.png"
    caption="Paraview pipeline"
    alt="Paraview pipeline figure 40"
    >}}