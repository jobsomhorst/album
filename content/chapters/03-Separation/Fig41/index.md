---
title: "Fig 41. Circular cylinder at R=13.1"
date: 2020-10-08
weight: 41
tags: ["StarCCM", "Laminar", "FlowPastCylinder", "Separation"]
authors:
  - "jobsomhorst"
---

{{< katex >}}

{{< slider "Featured.jpg" "new41.png" "Experiment" "Simulation">}}
*The standing eddies become elongated in the flow direction as the speed increases. Their length is found to increase linearly with Reynolds number until the flow becomes unstable above R=40.* Photograph by Sadathoshi Taneda

## General Info
This post is part of a series of posts about flow past a circular cylinder at different Reynolds numbers. In this post, it is clearly visible that the vortices behind the cylinder, which appear due to separation, grew in comparison to Figure 40. Also, one can see that the flow separates at an earlier point on the cylinder. More information about separation can be found in the [web post from Figure 40]({{< ref "/chapters/03-Separation/Fig40" >}}). 

The CFD simulation program used to perform the simulation of this figure is Simcenter STARCCM+. The visualization of the figure is done with Paraview, mainly using the Streamtracer filter. 720 particles are inserted at the circumference of the circle to colour it white. For this, a Matlab script is used in combination with a TableToPoints filter. The circle of particles is used as the source for a Particletracer filter, which is in its turn used to visualize the vortices behind the circle. The script and pipeline of Paraview and more information about the simulation and visualization can be found in the [web post from Figure 40]({{< ref "/chapters/03-Separation/Fig40" >}}).
