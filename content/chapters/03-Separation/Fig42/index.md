---
title: "Fig 42. Circular cylinder at R=26"
date: 2020-10-08
weight: 42
tags: ["StarCCM", "Laminar", "FlowPastCylinder", "Separation"]
authors:
  - "jobsomhorst"
---

{{< katex >}}

{{< slider "Featured.jpg" "new42.png" "Experiment" "Simulation">}}
*The downstream distance to the cores of the eddies also increases linearly with Reynolds number. However, the lateral distance between the cores appears to grow more nearly as the square root.* Photograph by Sadathoshi Taneda

## General Info
This post is part of a series of posts about flow past a circular cylinder at different Reynolds numbers. In this post, it is clearly visible that the vortices behind the cylinder, which appear due to separation, grew in comparison to earlier figures in this series. Also, one can see that the flow separates at an earlier point on the cylinder. More information about separation can be found in the [web post from Figure 40]({{< ref "/chapters/03-Separation/Fig40" >}}). 

The CFD simulation program used to perform the simulation of this figure is Simcenter STARCCM+. The visualization of the figure is done with Paraview, mainly using the Streamtracer filter. A lot of particles are inserted at the circumference of the circle to colour it white. For this, a Matlab script is used in combination with a TableToPoints filter. The circle of particles is used as the source for a Particletracer filter, which is in its turn used to visualize the vortices behind the circle. The script and pipeline of Paraview and more information about the simulation and visualization can be found in the [web post from Figure 40]({{< ref "/chapters/03-Separation/Fig40" >}}).
