---
title: "Fig 45. Circular cylinder at R=28.4"
date: 2020-10-08
weight: 45
tags: ["StarCCM", "Laminar", "FlowPastCylinder", "Separation"]
authors:
  - "jobsomhorst"
---

{{< katex >}}

{{< slider "Featured.jpg" "new45.4.png" "Experiment" "Simulation">}}
*Here just the boundary of the recirculating region has been made visible by coating the cylinder with condensed milk and setting it in motion through water.* Photograph by Sadathoshi Taneda

## General Info
This post is part of a series of posts about flow past a circular cylinder at different Reynolds numbers. In this post, it is clearly visible that the vortices behind the cylinder, which appear due to separation, grew in comparison to earlier figures in this series. Also, one can see that the flow separates at an earlier point on the cylinder. More information about separation can also be found in the [web post from Figure 40]({{< ref "/chapters/03-Separation/Fig40" >}}). 

The CFD simulation program used to perform the simulations of this figure is Simcenter STARCCM+. The visualization of the figure is done with Paraview, mainly using the Particletracer filter with a disk source as a seed source. Sadathoshi Taneda visualized this flow by coating the cylinder with condensed milk and setting it in motion through water. To match this, only a part of the total time steps were saved and the Particletracer filter was only used on these certain time steps. Therefore, only this small part of the complete simulation time was visualized. The figure below shows the vortices that appear behind the cylinder. This visualization was made using a Particletracer filter on all time steps. More information about the simulation can be found in the [web post from Figure 40]({{< ref "/chapters/03-Separation/Fig40" >}}).

{{< figure     
    src="new45.2.png"
    caption="Vortices Figure 45"
    alt="fig45 Vortices"
    >}}
