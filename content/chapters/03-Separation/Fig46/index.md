---
title: "Fig 46. Circular cylinder at R=41"
date: 2020-10-08
weight: 46
tags: ["StarCCM", "Laminar", "FlowPastCylinder", "Separation"]
authors:
  - "jobsomhorst"
---

{{< katex >}}

{{< slider "Featured.jpg" "new46.png" "Experiment" "Simulation">}}
*This is the approximate upper limit for steady flow. Far downstream the wake has already begun to oscillate sinusoidally. Tiny irregular gathers are appearing on the boundary of the recirculating region, but dying out as they reach its downstream end.* Photograph by Sadathoshi Taneda

## General Info
This post is part of a series of posts about flow past a circular cylinder at different Reynolds numbers. In this post, it is clearly visible that the vortices behind the cylinder, which appear due to separation, grew in comparison to earlier figures in this series. Also, one can see that the flow separates at an earlier point on the cylinder. More information about separation can also be found in the [web post from Figure 40]({{< ref "/chapters/03-Separation/Fig40" >}}). 

The caption also suggests that this is approximately the upper limit for steady flow. When increasing the Reynolds number, the flow will start to oscillate and develop into a periodic state. More information about this can be found in the [web post from Figure 94]({{< ref "/chapters/04-Vortices/Fig94" >}}).

The CFD simulation program used to perform the simulations of this figure is Simcenter STARCCM+. The visualization of the figure is done with Paraview, mainly using the Particletracer filter with a disk source as a seed source. More information about the visualization can be found in the [web post from Figure 45]({{< ref "/chapters/03-Separation/Fig45" >}}) and more information about the simulation can be found in the [web post from Figure 40]({{< ref "/chapters/03-Separation/Fig40" >}}).