---
title: "Fig 94. Kármán vortex street behind a circular cylinder at R=140"
date: 2020-10-08
weight: 94
tags: ["StarCCM", "Laminar", "FlowPastCylinder", "VortexShedding"]
authors:
  - "jobsomhorst"
---

{{< katex >}}

{{< slider "Featured.jpg" "new94.jpg" "Experiment" "Simulation">}}
*Water is flowing at 1.4 cm/s past a cylinder of diameter 1cm. Integrated streamlines are shown by electrolytic precipitation of white colloidal smoke, illuminated by a sheet of light. The vortex street is seen to grow in width downstream for some diameters.* Photograph by Sadathoshi Taneda

## Theory

#### Laminar, unsteady flow
The flow past a circular cylinder is a well-studied case in fluid dynamics. The flow pattern behind the cylinder changes when the Reynolds number of the flow changes. In the case of Figure 94, the Reynolds number is still relatively low, which represents a laminar. steady-state flow. The flow remains in a steady state until it reaches a Reynolds number of about \\(40\\). [Figure 96]({{< ref "/chapters/04-Vortices/Fig96" >}}). represents the upper limit of steady-state flow behind a circular cylinder. After this upper limit, an unstable wake forms because of the disappearing symmetry between the upstream and downstream, causing the flow to go to an unsteady state. Figure 94 is an example of unsteady-state flow behind a circular cylinder.

#### Reynolds number
The Reynolds number is a dimensionless number which can help to define fluid patterns by measuring the ratio between inertial and viscous forces. A low Reynolds number means that inertial forces are dominated by viscous forces, giving a laminar flow. High Reynolds numbers describe the opposite, a turbulent flow. The Reynolds number can be calculated with the use of the equation below.

$$
Re = \frac{\rho v L}{\mu}
$$

#### Kármán vortex street - Vortex shedding
Numerous studies found that the flow behind a circular cylinder is steady up until a certain point,
the Reynolds number at this point is called the critical Reynolds number (Sreenivasan, 1987). Beyond this critical Reynolds number, the flow starts to oscillate and develops into a periodic state. This state is called the Von Kármán vortex street and consists of two rows of staggered vortices of opposite directions (as can be seen in the Figure). 

#### Hopf Bifurcation
The point at which the stability of a system switches and a periodic solution arises is called a Hopf Bifurcation. It is a fixed critical point in a system where the system loses its stability. For flow behind a circular cylinder, the first Hopf Bifurcation arises when the Reynolds number reaches its critical point, \\(Re = 46\\).  Stable periodic solutions arise for a Reynolds number higher than its critical value (Figure below: c-d), until a second Bifurcation point which is at a Reynolds number of approximately 189. After this second Bifurcation point, the flow will become turbulent and the pattern disappears into a chaotic flow (Figure below: e). 

{{< figure     
    src="changesflow.jpg"
    caption="The change in flow when increasing the Reynolds number (Duyff,2006)"
    alt="fig94 re"
    >}}

Examples of every flow behind a circular cylinder flow type can be found in van Dyke's book and on this website. Take for example Figure 24 for attached flow (a), Figure 40 for separated flow (b), Figure 46 for the start point at which stable periodic solutions arise (c), Figure 94 for von Kármán streets (d) and Figure 47 for chaotic flow (e).

#### Strouhal number
The periodic flow that arises after a Reynolds number of \\(46\\) can be described with a dimensionless number, the Strouhal Number. The Strouhal number is described by the equation below where \\(f\\) is the frequency of vortex shedding, \\(d\\) is the diameter of the cylinder and \\(U\\) is the flow velocity. 

$$
St = fd/U
$$

At high Strouhal numbers (\\(St > 0.3)\\), oscillations dominate the flow. At lower Strouhal numbers (\\(0.15-0.3)\\), vortex shedding takes place.  

The Strouhal number has a dependency on the Reynolds number but is for a large Reynolds number interval (\\(250 < Re < 2 \cdot 10^5\\)) approximately constant and equal to \\(0.2\\) as can be seen in the figure below.

{{< figure     
    src="STRe.png"
    caption="Strouhal number vs Reynolds number for flow behind a circular cylinder (Belvins,1990)"
    alt="fig94 revsst"
    >}}

#### Laminar to turbulent transition
When the flow past a circular cylinder is laminar, the drag coefficient (\\(C_D = \frac{D}{1/2 \cdot \rho \cdot u^2 \cdot A }\\)) is relatively high. The reason for this high drag coefficient is the fact that laminar flow is a smooth flow. This results in a larger wake region and higher drag. The infinitesimal boundary layer that moves closest to the cylinder stays attached to the cylinder for a longer period of time, causing the drag force to be relatively high.\newline

Turbulent flow is, opposite to laminar flow, chaotic and irregular fluid motion. Because of this, the formation of the large wake region gets disrupted and one experiences increased mixing and the appearance of turbulent eddies in the flow. This is the reason that, when flow switches from a laminar to a turbulent state, the boundary layer detaches earlier, causing the drag coefficient to be relatively low. A turbulent flow is characterized by a higher Reynolds number. [Figure 47]({{< ref "/chapters/03-Separation/Fig47" >}}) shows the flow past a circular cylinder for a turbulent flow.
    
## Simulation
The CFD simulation program used to perform the above simulations is Simcenter StarCCM+. The flow models used in the simulations are valid for 2D cases, thus the simulations are run in 2D to decrease the computational time.

#### Boundary conditions and computational domain
The Reynolds number (\\(Re = 140\\)), velocity (\\(u = 1.4 cm/s = 0.0014 m/s\\)) and cylinder diameter (\\(D = 1 cm\\)) were given in the caption of the original figure. From here, the computational domain and boundary conditions were created.

The computational domain can be seen in the figure below. The total domain has a length of \\(19 cm\\) and the distance from the centre of the cylinder to the end of the domain is \\(16 cm\\), leaving \\(3 cm\\) in front of the centre. The height of the domain is \\(10 cm\\), with the centre of the cylinder at \\(5 cm\\). The total domain of the Figure was not given in the caption but derived from the original figure. The solution of the simulation is not affected by the scale of the domain, therefore the top and bottom of the domain are set as symmetry planes. The cylinder is set as a wall with a no-slip boundary condition applied to it. The inflow is set as a velocity inlet with a velocity of \\(0.0014 m/s\\) and the outlet is set as a pressure outlet.

{{< figure     
    src="cd94.png"
    caption="Computational domain, boundary conditions and mesh."
    alt="computational domain 94"
    >}}

#### Meshing
The mesh created for this simulation is a quadrilateral 2D mesh, which is one of many meshing options from STARCCM+. After selecting the quadrilateral 2D mesh, the polygonal mesh option is used. This mesh consists of polyhedral-shaped cells. In comparison to an equivalent tetrahedral mesh, a polygonal mesh contains five times fewer cells and is more accurate, more stable and less diffusive. The base size of the mesh is set to \\(0.02 m\\) and the growth rate to \\(1.004\\). A prism layer mesh is created next to the cylinder. Because of this, the accuracy is higher close to the cylinder. A total amount of 135212 cells were created. Some representations of the mesh can be found below.

{{< carousel images="mesh*.jpg" interval="3000">}}

#### STARCCM+ Setup
The simulation was run with a laminar, unsteady model with water as the flowing fluid. For the simulation, a second-order implicit unsteady solver with a time-step of \\(0.025 sec\\) was used. A total of 1400 time steps were run with 50 iterations per time step, resulting in 70000 iterations and a physical time of 35 sec. The velocity data of all time steps was exported to an Ensight Gold case file, so it could be processed in Paraview. All models used in STARCCM+ and the STARCCM+ result after 1400 time steps can be found below.

{{< carousel images="94*.png" interval="3000">}}

#### Visualization
Paraview has been used for the post-processing and visualization of the simulation. After importing the velocity data, a calculator filter has been used to compute the velocity vector from the x-velocity and y-velocity data that was exported. After this, the circumference of the cylinder is covered in particles with the code below. In combination with a TableToPoints filter, the particles are used as an input seed for the Particletracer filter. The Particletracer filter, with the particles created with the TableToPoints filter as an input seed and the calculator as the input, is used to compute the final simulation shown at the start of this web post. The code used for creating the particles at the circumference of the cylinder can be found in the web post from Figure 40. The Paraview pipeline can be seen in the figure below.

{{< figure     
    src="pipeline94.1.png"
    caption="Paraview pipeline"
    alt="Pipeline 94"
    >}}