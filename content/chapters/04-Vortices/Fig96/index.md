---
title: "Fig 96. Kármán vortex street behind a circular cylinder at R=105"
date: 2020-10-08
weight: 96
tags: ["StarCCM", "Laminar", "FlowPastCylinder", "VortexShedding"]
authors:
  - "jobsomhorst"
---

{{< katex >}}

{{< slider "Featured.jpg" "new96.png" "Experiment" "Simulation">}}
*The initially spreading wake shown opposite develops into the two parallel rows of staggered vortices that von Kármàn's inviscid theory shows to be stable when the ratio of width to streamwise spacing is 0.28. Streaklines are shown by electrolytic precipitation in water.* Photograph by Sadathoshi Taneda

## General Info
This post is part of a series of posts about flow past a circular cylinder at different Reynolds numbers. In this post, one finds a longer version of the von Kármán Street shown in Figure 94. In comparison to Figure 94, the photograph of this street is taken at a later time step. The vortices in this figure appear at the same height of the domain, while the vortices in Figure 94 appear at increasing heights. When a later time step of the flow in Figure 94 is taken, the vortices also even out and appear at the same height.

The computational domain differs in length and height from Figure 94, the domain can be found in the Figure below. More information about the theory behind this figure can be found in the [web post from Figure 94]({{< ref "/chapters/04-Vortices/Fig94" >}}).

{{< figure     
    src="cd96.png"
    caption="Computational domain, boundary conditions and mesh."
    alt="fig96 domain"
    >}}

The CFD simulation program used to perform the simulations of this figure is Simcenter STARCCM+. The visualization of the figure is done with Paraview, mainly using the Particletracer filter. 720 particles are inserted at the circumference of the circle. For this, a Matlab script is used in combination with a TableToPoints filter. The circle of particles is used as the source for a Particletracer filter, which is in turn used to visualize the vortices behind the circle. The script and pipeline of Paraview and more information about the simulation and visualization can also be found in the [web post from Figure 94]({{< ref "/chapters/04-Vortices/Fig94" >}}).
